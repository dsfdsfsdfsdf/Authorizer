﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dillon.Security
{
    public class RC2RSA
    {
        public RC2RSA()
        {

        }

        public static string Encrypt(string value, string d, string n)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            var keyIV = Aes.GetKeyIV();
            var encryptKey = RSA.Encrypt(keyIV.Key, d, n);
            var encryptIV = RSA.Encrypt(keyIV.IV, d, n);
            var rc2Encypt = Aes.Encrypt(value, keyIV.Key, keyIV.IV);
            return string.Concat(encryptKey, "-", encryptIV, "-", rc2Encypt);
        }


        public static string Decrypt(string value, string e, string n)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            var pos = value.IndexOf('-');
            if (pos <= 0)
            {
                throw new Exception("");
            }
            var pos2 = value.IndexOf('-', pos + 1);
            if (pos2 - pos <= 0)
            {
                throw new Exception("");
            }


            var decryptKey = value.Substring(0, pos);
            var decryptIV = value.Substring(pos + 1, pos2 - pos - 1);
            var decryptValue = value.Substring(pos2 + 1);
            var key = RSA.Decrypt(decryptKey, e, n);
            var iv = RSA.Decrypt(decryptIV, e, n);

            return Aes.Decrypt(decryptValue, key, iv);
        }
    }
}
